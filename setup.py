# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name="FloodTags SDK",
    version="1.5.2",
    packages=['floodtags_sdk'],
    package_data={
        'floodtags_sdk': [
            'iana_timezones.txt',
            'default_config.yaml',
            'test_sources.json',
            'schemas/adapter_v0.1.json',
            'schemas/adapter_v0.2.json',
            'schemas/tag_v1.0.json',
            'schemas/tag_v1.1.json',
            'schemas/extensions/flood-image-analysis-v0.1.json',
            'schemas/extensions/news-event-information-extraction-v0.1.json'
        ]
    },
    install_requires=['requests', 'pyyaml', 'fastjsonschema', 'pymongo'],
    zip_safe=False,
    author="FloodTags",
    author_email="info@floodtags.com",
    description="Provides the interfaces between the FloodTags systems and Python Code",
)
