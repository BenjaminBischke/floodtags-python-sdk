# FloodTags Python SDK
This repository hosts the 'floodtags_sdk' python package, targeted at
developers who want to develop code for use in the FloodTags systems

## Installation
The package can be installed using PIP:
`pip install git+https:/bitbucket.org/floodtags/floodtags-python-sdk.git@v1.4.4`

### Configuration
Configuration of the package is done through a YAML file. A template for this
file can be found [here](config/template.yaml). This file should be adapted to
specific needs, and copied to a configuration directory, that is defined by the
`$CONFIG_DIR` environment variable. When running a script that uses this
package, it should always be run in an environment with this variable, and the
yaml file should be stored in this directory as 'floodtags_sdk.yaml'. When
running in production, FloodTags will replace this with it's own configuration
files. Each of the config parameters is documented in the template file

## Usage
Below are some scenario's and examples where the FloodTags SDK can be applied

### Quick validation of data formats:
To validate tags:

```python
import json
from floodtags_sdk import formats

tag_data = json.loads(tag_json_str)

formats.validator.validate(tag_data, 'tag', '1.1')
```

Validating payloads send by the flood image analysis algorithm to the adapter:
```python
formats.validator.validate(payload_dict, 'adapter', 'latest', extension='flood-image-analysis', extension_version='latest')
```
The above combines the basic schema of the adapter, with the specific schema
for the enrichment (see floodtags_sdk/schemas/extensions)

### Logging
When delivering python scripts to be used on FloodTags servers, please use the
`floodtags_sdk.logging.get_logger()` function to get the root logger in the
main script. This will set-up logging to JSON-lines files, so we can easily
analyze and monitor them. For example, in your main script:

```python
from floodtags_sdk import logging

# The only required parameter is the id of the script, so logs can be
# identified. Although you can pass a log level, please only use this in
# debugging, since the log level by default is governed by the YAML config
logger = logging.get_logger('flood-image-analysis')

logger.info('Created the root logger')
```
Using this function will (1) make sure logs end-up in the correct
directory on the FloodTags servers, (2) logs are in the correct format, so we
can ship it to ElasticSearch and monitor them and (3) Fatal errors causing the
script to crash are logged at 'CRITICAL' level, so we can monitor this.

These logs are stored in json-lines files, in the location you have specified
in the floodtags_sdk.yaml config file.

### Analysis Algorithms
If you're developing algorithms that analyse FloodTags data, but take a longer
time to produce results (multiple seconds or more), these will be connected as
external algorithms in the FloodTags system. This means a server should be
listening to POST requests from the FloodTags system, and send it's results to
the FloodTags adapter

#### Receiving data
In Python, the JSON data in this POST-request, can be converted to a
dictionary. When using data from the FloodTags systems, please use this
dictionary to create a `floodtags_sdk.formats.Tag` object:

```python
from floodtags_sdk.formats import Tag
import json

tag_dict = json.loads(request_payload) # Converting incoming data to a dict()
tag = Tag.from_dict(tag_dict, as_version='1.1')
```

This will create a `Tag` object from the data that the algorithm receives. The
advantage of using this over the bare JSON payload is that it validates the
input using JSON-schema and it can handle different versions of input being
sent. This means that for example, if FloodTags will update the format sent
through POST requests, the Tag in your script will offer the same data, as long
as the same 'as_version' is specified.

_Note: When starting developing, please use the newest version, since this
the full FloodTags data format_

The Tag object is implemented as a `collections.abc.MutableMapping` meaning it
offers the same functionality as a dictionary, e.g.
`print(tag['source']['type'])` will print the source.type of the tag.

#### Sending data
Results of analyses can be sent to the adapter using the `adapter.send()`
method as illustrated below:

```python
from floodtags_sdk import adapter

# Note that with adapter.send(), you can send both a single operation, such as
# below (dict), or multiple in a list. The 'payload' parameter takes both
sample_payload = {
  "type": "tag",
  "id": "news-DemocraticVoiceOfBurmaEnglish-76466",
  "operations": [
    {
      "type": "replace",
      "key": "analyzedPhotos",
      "value": [
        {
          "url": "http://english.dvb.no/wp-content/uploads/2017/07/Khantee-water-flooat-1-622x350.jpg",
          "caption": "Floodwaters are rising in Upper Sagaing region, 10 July 2017. (PHOTO: DVB)",
          "analysisResults": {
            "analysisTimestamp": "2017-07-01T11:11:10Z",
            "floodRelated": True,
            "imagePreviouslyUsed": False,
            "personDepthEvidence": True,
            "personPresent": True,
            "transportMotorizedPresent": False,
            "transportNonMotorizedPresent": False,
            "transportWaterPresent": False,
            "buildingPresent": False,
            "responseWorkerPresent": False,
            "cattlePresent": False,
            "urbanization": "low"
          }
        }
      ]
    }
  ]
}

adapter.send(sample_payload, 'flood-image-analysis')
```
This function will validate the format of the payload, and tries to send it to
the adapter. If the adapter is offline, the POST-request data are stored
locally in a json-lines file, and an ERROR is logged