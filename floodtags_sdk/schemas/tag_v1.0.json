{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "title": "A FloodTags Tag",
    "required": [
        "date",
        "filterSources",
        "source",
        "id",
        "media",
        "locations"
    ],
    "properties": {
        "date": {
            "$id": "#/properties/date",
            "type": "string",
            "description": "Post Date of the Tag (UTC)",
            "examples": [
                "2018-01-01T00:00:00Z"
            ],
            "format": "date-time"
        },
        "filterSources": {
            "$id": "#/properties/filterSources",
            "type": "array",
            "description": "The Filter Sources a Tag belongs to",
            "items": {
                "$id": "#/properties/filterSources/items",
                "type": "string",
                "description": "A filter source",
                "examples": [
                    "flood",
                    "indonesia"
                ]
            }
        },
        "source": {
            "$id": "#/properties/source",
            "type": "object",
            "description": "Data about the source of the Tag",
            "required": [
                "type",
                "id"
            ],
            "additionalProperties": false,
            "properties": {
                "type": {
                    "$id": "#/properties/source/properties/type",
                    "type": "string",
                    "enum": [
                        "twitter",
                        "news",
                        "forum",
                        "telegram",
                        "whatsapp",
                        "bot"
                    ],
                    "description": "The Type of source"
                },
                "name": {
                    "$id": "#/properties/source/properties/name",
                    "type": "string",
                    "description": "Name of the source (If not the same as source.type)",
                    "examples": [
                        "New York Times"
                    ]
                },
                "id": {
                    "$id": "#/properties/source/properties/id",
                    "type": "string",
                    "description": "The Id of the tag on the source platform"
                },
                "userId": {
                    "$id": "#/properties/source/properties/userId",
                    "type": "string",
                    "description": "Id of the user posting the tag on source platform"
                },
                "isReply": {
                    "$id": "#/properties/source/properties/isReply",
                    "type": "boolean",
                    "description": "Whether the tag is a reply to another tag"
                },
                "repliesTo": {
                    "$id": "#/properties/source/properties/repliesTo",
                    "description": "The message that this Tag replies to",
                    "$ref": "#/definitions/messageRelation"
                },
                "isQuote": {
                    "$id": "#/properties/source/properties/isQuote",
                    "type": "boolean",
                    "description": "Whether the tag quotes another tag"
                },
                "quotes": {
                    "$id": "#/properties/source/properties/quotes",
                    "description": "The message that Tag quotes",
                    "$ref": "#/definitions/messageRelation"
                },
                "isReplica": {
                    "$id": "#/properties/source/properties/isReplica",
                    "type": "boolean",
                    "description": "Whether the tag is a replica of another tag"
                },
                "replicates": {
                    "$id": "#/properties/source/properties/replicates",
                    "description": "The message that this tag replicates",
                    "$ref": "#/definitions/messageRelation"
                },
                "mentionsUsers": {
                    "$id": "#/properties/source/properties/mentionsUsers",
                    "type": "array",
                    "description": "The users mentioned in the message",
                    "items": {
                        "$id": "#/properties/source/properties/mentionsUsers/items",
                        "type": "string",
                        "description": "A user id on the source platform"
                    }
                }
            }
        },
        "id": {
            "$id": "#/properties/id",
            "type": "string",
            "description": "Unique Id of the tag in the FloodTags system",
            "pattern": "^(t|w|f|news|tg|bot)-[^:\\s]*$",
            "examples": [
                "t-1143209649317076993"
            ]
        },
        "author": {
            "$id": "#/properties/author",
            "type": "object",
            "description": "Author Information",
            "additionalProperties": false,
            "properties": {
                "id": {
                    "$id": "#/properties/author/properties/id",
                    "type": "string",
                    "description": "The Author's Id in the FloodTags Systems",
                    "pattern": "^(t|w|f|news|tg|bot|phone)-[^:\\s]*$",
                    "examples": [
                        "phone-3155668965"
                    ]
                },
                "description": {
                    "$id": "#/properties/author/properties/description",
                    "type": "string",
                    "description": "A description of the author"
                },
                "creationDate": {
                    "$id": "#/properties/author/properties/creationDate",
                    "type": "string",
                    "description": "The author creation date",
                    "format": "date-time"
                },
                "utcOffset": {
                    "$id": "#/properties/author/properties/utcOffset",
                    "type": "integer",
                    "description": "UTC offset of the author",
                    "minimum": -43200,
                    "maximum": 50400
                },
                "timezone": {
                    "$id": "#/properties/author/properties/timezone",
                    "type": "string",
                    "description": "The IANA timezone of the author"
                },
                "location": {
                    "$id": "#/properties/author/properties/location",
                    "oneOf": [
                        {
                            "type": "object",
                            "description": "Location of author, location name only",
                            "required": [
                                "properties"
                            ],
                            "additionalProperties": false,
                            "properties": {
                                "properties": {
                                    "type": "object",
                                    "additionalProperties": false,
                                    "required": [
                                        "name"
                                    ],
                                    "properties": {
                                        "name": {
                                            "type": "string",
                                            "description": "name of the location"
                                        }
                                    }
                                }
                            }
                        },
                        {
                            "type": "object",
                            "description": "Location of author, with geometry",
                            "required": [
                                "type",
                                "geometry"
                            ],
                            "additionalProperties": false,
                            "properties": {
                                "type": {
                                    "$id": "#/properties/locations/items/properties/type",
                                    "description": "GeoJSON type, always a feature",
                                    "type": "string",
                                    "enum": [
                                        "Feature"
                                    ]
                                },
                                "geometry": {
                                    "$id": "#/properties/locations/items/properties/geometry",
                                    "description": "A GeoJSON geometry",
                                    "$ref": "https://geojson.org/schema/Geometry.json"
                                },
                                "properties": {
                                    "$id": "#/properties/locations/items/properties/properties",
                                    "type": "object",
                                    "description": "Location properties",
                                    "properties": {
                                        "id": {
                                            "$id": "#/properties/locations/items/properties/properties/properties/id",
                                            "type": "string",
                                            "description": "Unique identifier for the location"
                                        },
                                        "type": {
                                            "$id": "#/properties/locations/items/properties/properties/properties/type",
                                            "type": "string",
                                            "description": "The type of location",
                                            "enum": [
                                                "GPS",
                                                "user_picked"
                                            ]
                                        },
                                        "name": {
                                            "$id": "#/properties/locations/items/properties/properties/properties/name",
                                            "type": "string",
                                            "description": "The name of the location"
                                        }
                                    },
                                    "additionalProperties": true
                                }
                            }
                        }
                    ]
                }
            }
        },
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "description": "Title of a Tag (e.g. for news articles)",
            "examples": [
                "Flood Kills 5 in Dar es Salaam"
            ]
        },
        "text": {
            "$id": "#/properties/text",
            "type": "string",
            "description": "Body text of a tag",
            "examples": [
                "Yesterday's flooding of the city of Dar es Salaam in Tanzania has caused great damages"
            ]
        },
        "summary": {
            "$id": "#/properties/summary",
            "type": "string",
            "description": "A summary (e.g. if provided with a news article)",
            "examples": [
                "The prime minister visited the flood affected areas yesterday and promised 10 billion in aid"
            ]
        },
        "url": {
            "$id": "#/properties/url",
            "type": "string",
            "description": "If applicable, the URL of the Tag",
            "format": "uri",
            "examples": [
                "https://twitter.com/BBCScotlandNews/status/1143209649317076993"
            ]
        },
        "media": {
            "$id": "#/properties/media",
            "type": "object",
            "description": "Information about media related to the Tag",
            "required": [
                "photos",
                "videos",
                "documents",
                "audio",
                "urls"
            ],
            "additionalProperties": false,
            "properties": {
                "photos": {
                    "$id": "#/properties/media/properties/photos",
                    "type": "array",
                    "description": "Photographs attached to the Tag",
                    "items": {
                        "$id": "#/properties/media/properties/photos/items",
                        "description": "A Photograph URL",
                        "$ref": "#/definitions/URL"
                    }
                },
                "videos": {
                    "$id": "#/properties/media/properties/videos",
                    "type": "array",
                    "description": "Video's attached to the Tag",
                    "items": {
                        "$id": "#/properties/media/properties/videos/items",
                        "description": "A Video URL",
                        "$ref": "#/definitions/URL"
                    }
                },
                "documents": {
                    "$id": "#/properties/media/properties/documents",
                    "type": "array",
                    "description": "Documents attached to the Tag",
                    "items": {
                        "$id": "#/properties/media/properties/documents/items",
                        "description": "A Document URL",
                        "$ref": "#/definitions/URL"
                    }
                },
                "audio": {
                    "$id": "#/properties/media/properties/audio",
                    "type": "array",
                    "description": "Audio fragments related to the Tag",
                    "items": {
                        "$id": "#/properties/media/properties/audio/items",
                        "description": "A Audio Fragment URL",
                        "$ref": "#/definitions/URL"
                    }
                },
                "urls": {
                    "$id": "#/properties/media/properties/urls",
                    "type": "array",
                    "description": "URLs mentioned in the Tag",
                    "items": {
                        "$id": "#/properties/media/properties/urls/items",
                        "description": "A URL mentioned in the tag",
                        "$ref": "#/definitions/URL"
                    }
                }
            }
        },
        "language": {
            "$id": "#/properties/language",
            "type": "string",
            "description": "Two letter language code of the Tag (ISO 639-1)",
            "examples": [
                "en"
            ],
            "pattern": "^[a-z]{2}$"
        },
        "locations": {
            "$id": "#/properties/locations",
            "type": "array",
            "description": "Locations shared with the Tag",
            "items": {
                "$id": "#/properties/locations/items",
                "type": "object",
                "description": "A location",
                "required": [
                    "type",
                    "geometry",
                    "properties"
                ],
                "additionalProperties": false,
                "properties": {
                    "type": {
                        "$id": "#/properties/locations/items/properties/type",
                        "description": "GeoJSON type, always a feature",
                        "type": "string",
                        "enum": [
                            "Feature"
                        ]
                    },
                    "geometry": {
                        "$id": "#/properties/locations/items/properties/geometry",
                        "description": "A GeoJSON geometry",
                        "$ref": "https://geojson.org/schema/Geometry.json"
                    },
                    "properties": {
                        "$id": "#/properties/locations/items/properties/properties",
                        "type": "object",
                        "description": "Location properties",
                        "required": [
                            "id",
                            "type"
                        ],
                        "properties": {
                            "id": {
                                "$id": "#/properties/locations/items/properties/properties/properties/id",
                                "type": "string",
                                "description": "Unique identifier for the location"
                            },
                            "type": {
                                "$id": "#/properties/locations/items/properties/properties/properties/type",
                                "type": "string",
                                "description": "The type of location",
                                "enum": [
                                    "GPS",
                                    "user_picked"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/locations/items/properties/properties/properties/name",
                                "type": "string",
                                "description": "The name of the location"
                            }
                        },
                        "additionalProperties": true
                    }
                }
            }
        },
        "waterDepth": {
            "$id": "#/properties/waterDepth",
            "type": "integer",
            "description": "Water Depth of the Tag (cm)",
            "minimum": 1,
            "maximum": 1000
        },
        "topics": {
            "$id": "#/properties/topics",
            "type": "array",
            "description": "Topic of the tag",
            "items": {
                "$id": "#/properties/topics/items",
                "type": "string",
                "enum": [
                    "flood",
                    "drought"
                ],
                "description": "A topic"
            }
        },
        "hashtags": {
            "$id": "#/properties/hashtags",
            "type": "array",
            "description": "List of hashtags mentioned in the tag",
            "items": {
                "$id": "#/properties/hashtags/items",
                "type": "string",
                "description": "A hashtag",
                "pattern": "^#[^\\s]*$"
            }
        }
    },
    "definitions": {
        "messageRelation": {
            "type": "object",
            "additionalProperties": false,
            "properties": {
                "id": {
                    "type": "string",
                    "description": "The Id of the related message on the source platform"
                },
                "userId": {
                    "type": "string",
                    "description": "The id of the user that posted the related message on the source platform"
                }
            }
        },
        "URL": {
            "type": "string",
            "format": "uri"
        },
        "geojsonGeometry": {
            "oneOf": [
                {
                    "title": "GeoJSON Point",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "Point"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "minItems": 2,
                            "items": {
                                "type": "number"
                            }
                        }
                    }
                },
                {
                    "title": "GeoJSON LineString",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "LineString"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "minItems": 2,
                            "items": {
                                "type": "array",
                                "minItems": 2,
                                "items": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                },
                {
                    "title": "GeoJSON Polygon",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "Polygon"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "minItems": 4,
                                "items": {
                                    "type": "array",
                                    "minItems": 2,
                                    "items": {
                                        "type": "number"
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    "title": "GeoJSON MultiPoint",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "MultiPoint"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "minItems": 2,
                                "items": {
                                    "type": "number"
                                }
                            }
                        }
                    }
                },
                {
                    "title": "GeoJSON MultiLineString",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "MultiLineString"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "minItems": 2,
                                "items": {
                                    "type": "array",
                                    "minItems": 2,
                                    "items": {
                                        "type": "number"
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    "title": "GeoJSON MultiPolygon",
                    "type": "object",
                    "required": [
                        "type",
                        "coordinates"
                    ],
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "MultiPolygon"
                            ]
                        },
                        "coordinates": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "items": {
                                    "type": "array",
                                    "minItems": 4,
                                    "items": {
                                        "type": "array",
                                        "minItems": 2,
                                        "items": {
                                            "type": "number"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        }
    }
}
