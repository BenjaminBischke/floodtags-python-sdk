# -*- coding: utf-8 -*-
"""
Logging module, with functions to setup logging for the FloodTags systems.
Each line of the logging file is valid JSON, making the logging file a
json-lines file
"""
import logging
from logging import handlers
import datetime
import json
import sys
import os

from floodtags_sdk import config


class Formatter(logging.Formatter):
    """
    Formatter for creating JSON lines log files for elasticsearch

    Arguments:
        log_source_name --- str: The name if the script that is being logged
    """

    def __init__(self, log_source_name):
        # Set the source name
        self.s_name = log_source_name

    def get_error_info(self, record):
        """
        Gets an error information string, if an error has occured, otherwise
        returns None. Code partially from logging.Formatter
        """
        s = ' '
        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + record.exc_text
        if record.stack_info:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + self.formatStack(record.stack_info)

        if s == ' ':
            return None
        else:
            return s.strip()

    def format(self, record):
        """
        Format a log record to the json format
        """
        err = record.exc_info
        if err is False or err is True:
            # This is somehow the case for some elasticsearch package errors..
            error_type = None
        elif err is not None:
            error_type = record.exc_info[0].__name__
        else:
            error_type = None

        payload = {
            'level': record.levelname,
            'date': datetime.datetime.utcfromtimestamp(
                        record.created
                    ).isoformat(),
            'message': record.getMessage(),
            'source': self.s_name,
            'subModule': record.name,
            'errorType': error_type,
            'errorInfo': self.get_error_info(record)
        }

        return json.dumps(payload, ensure_ascii=False)


def get_logger(script_name, level=config.logging.level):
    """
    Get a root level logger, that logs all information using a rotating file
    handler to the LOG_DIR

    Arguments:
        script_name --- str: The name of the script that is being logger,
        preferably the filename without extension (used as local filename and
        'source' in the JSON/in ElasticSearch)

        level='WARNING' --- str: The minimum level to log (see
        https://docs.python.org/3/library/logging.html#logging-levels)

    Returns:
        logging.Logger --- The pre-configured root-logger
    """
    # Get root logger
    logger = logging.getLogger()

    # Determine file location, and set rotating file handler
    file_loc = os.path.join(config.dir.logs, script_name + '.log.jsonl')
    handler = handlers.RotatingFileHandler(file_loc,
                                           maxBytes=1048576,
                                           backupCount=4)

    loglevel = getattr(logging, level)
    # Set JSON formatter and log level
    formatter = Formatter(script_name)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(loglevel)

    # Set level the handler
    handler.setLevel(loglevel)

    # Replace the default exception printing function, so that unhandled
    # exceptions are logged before stopping the script
    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            logger.warning("STOPPED because of SIGINT signal")
        else:
            logger.critical(
                "A critical error occured",
                exc_info=(exc_type, exc_value, exc_traceback)
            )
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    sys.excepthook = handle_exception

    return logger
