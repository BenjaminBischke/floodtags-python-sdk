# -*- coding: utf-8 -*-
"""
Contains the functions related to sending data to the adapter
"""
from pathlib import Path
import logging

import requests

from floodtags_sdk import config
from floodtags_sdk._aux import save_json
from floodtags_sdk.formats import validator

logger = logging.getLogger(__name__)

url = 'http://{ip}:{port}/'.format(ip=config.adapter.ip,
                                   port=config.adapter.port)


def send(payload, source_id, validate_version='latest'):
    """
    Send a list of tags to the pipeline

    Arguments:
        payload --- A single tag/event update (dict) or a list of updates

        source_id --- str: The unique identifier of the algorithm sending
        the tags. This determines what JSON schema to use in validation

        validate_version='latest' --- str: The payload version to validate
        against. This determines what version of the JSON schema is used for
        validation. 'latest' takes the highest version available.
    """
    if isinstance(payload, dict):
        payload = [payload]

    # Each function call only logs a non-status code error once
    exception_logged = False

    for i, update in enumerate(payload):
        # Each update is sent seperately; This way we can save individual
        # entries if they yield a 500 error

        # Create the format for a single data payload, sent to the adapter
        single_payload = {
            'sourceId': source_id,
            'payload': update
        }

        try:
            # Verify if the payload matches the format for the specific algrthm
            validator.validate(single_payload, 'adapter', 'latest', source_id,
                               validate_version)

            response = requests.post(url,
                                     json=single_payload,
                                     timeout=config.adapter.timeout)

            # Make sure an exception is raised for an invalid status
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            # This catches the exception for an invalid status code
            save_path = Path(config.dir.unsent, 'adapter_processing_failed.jl')
            logger.exception(
                'The adapter returned a {} status code, storing data in {}.'
                ' See details below'.format(
                    response.status_code, save_path.as_posix()
                )
            )
            save_json(single_payload, save_path)

        except requests.exceptions.RequestException:
            # All other problems are handled here
            save_path = Path(config.dir.unsent, 'adapter_unsent.jl')
            if i == 0:
                # If it fails on the first element, adapter is unreachable, so
                # subsequent data is not even tried.
                for u in payload:
                    single_payload = {
                        'sourceId': source_id,
                        'payload': update
                    }
                    save_json(single_payload, save_path)

                # All is saved, don't try any subsequent sends...
                break
            else:
                # Only current payload is saved
                save_json(single_payload, save_path)

            if not exception_logged:
                logger.exception(
                    'Unable to send data to the adapter, storing it in {}.'
                    ' See details below'.format(
                        save_path.as_posix()
                    )
                )
                # Because probably all remaining entries will fail, this is
                # only logged once
                exception_logged = True
