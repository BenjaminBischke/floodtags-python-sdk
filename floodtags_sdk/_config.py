# -*- coding: utf-8 -*-
"""
Configuration for floodtags_sdk. To configure other than defaults, put a
'floodtags_sdk.yaml' file in the directory set as the CONFIG_DIR environment
variable
"""
from pathlib import Path
import logging
import os
import yaml
from collections import namedtuple
import re

logger = logging.getLogger(__name__)
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR' 'CRITICAL']

env_variable_matcher = re.compile(r'\$\{([^}^{]+)\}')


def env_variable_constructor(loader, node):
    """
    Expands environment variables in yaml configs
    """
    value = node.value
    match = env_variable_matcher.match(value)
    env_var = match.group(1)
    return os.environ.get(env_var) + value[match.end():]


yaml.add_implicit_resolver('!envvariable', env_variable_matcher,
                           Loader=yaml.SafeLoader)
yaml.add_constructor('!envvariable', env_variable_constructor,
                     Loader=yaml.SafeLoader)


def _validate_and_clean_dir(dirloc):
    loc = Path(dirloc).expanduser()
    if loc.is_dir():
        return loc
    else:
        raise NotADirectoryError(
            'Location {} is not a directory'.format(dirloc)
        )


class ServerConfig(namedtuple('ServerConfig', ['ip', 'port', 'timeout'])):
    """
    Pipeline configuration

    Args:
        url --- str:The url of the pipeline

        timeout --- int: The connect timeout for the pipeline
    """

    def __new__(cls, **kwargs):
        # Compatibility for old-style config, with URL:
        if 'url' in kwargs:
            match = re.match(r'http://([^:/]+)(:(\d+))?/', kwargs.pop('url'))
            kwargs['ip'] = match.group(1)
            port = match.group(3)
            if port is not None:
                kwargs['port'] = int(port)
            else:
                kwargs['port'] = 80

        if not isinstance(kwargs['timeout'], (int, float)):
            raise TypeError('.timeout should be a number')
        if not isinstance(kwargs['port'], (int, float)):
            raise TypeError('.port should be a number')
        return super().__new__(cls, **kwargs)


class Dir(namedtuple('Dir', ['logs', 'unsent', 'temp'])):
    """
    Directory configuration

    Args:
        logs --- str: The folder where log files are stored

        unsent --- str: The folder where tags are stored if they cannot be
        sent to the pipeline
    """

    def __new__(cls, **kwargs):
        # validate and rewrite kwargs
        for key, dirloc in kwargs.items():
            kwargs[key] = _validate_and_clean_dir(dirloc)
        return super().__new__(cls, **kwargs)


class DBConfig(namedtuple('DBConfig', ['url', 'username', 'password'])):
    """
    Single database configuration

    Args:
        url --- str: The URL of the database

        username --- str: Username to access the database

        password --- str: Password to access the database
    """

    def __new__(cls, **kwargs):
        found = set()
        must = set(['url', 'username', 'password'])
        for key, value in kwargs.items():
            found.add(key)
            if not isinstance(value, str) and value is not None:
                raise ValueError('Key {} should be string'.format(key))
        remaining = must - found
        for key in remaining:
            kwargs[key] = None
        return super().__new__(cls, **kwargs)


class Databases(namedtuple('Databases', ['settings'])):
    """
    Databases configuration

    Args:
        sources --- dict: The configuration for the connection to the sources
        database
    """

    def __new__(cls, **kwargs):
        # validate and rewrite kwargs
        # For old style config files, settings may be called sources:
        if 'sources' in kwargs:
            kwargs['settings'] = kwargs.pop('sources')
        # Now create a DBconfig for each db
        for key, db_dict in kwargs.items():
            kwargs[key] = DBConfig(**db_dict)
        return super().__new__(cls, **kwargs)


class Logging(namedtuple('Logging', ['level'])):
    """
    Pipeline configuration

    Args:
        level --- str: One of the python logging levels
    """

    def __new__(cls, **kwargs):
        if kwargs['level'] not in LOG_LEVELS:
            raise ValueError('Invalid value for logging.level')
        return super().__new__(cls, **kwargs)


class Config(namedtuple('Config', ['pipeline', 'dir', 'db', 'logging',
                                   'adapter'])):
    """
    floodtags_sdk config, loaded from YAML file

    Args:
        fileloc --- str: Location of YAML config file
    """

    def __new__(cls, fileloc):
        with open(fileloc, 'r', encoding="utf8") as configfile:
            config_data = yaml.safe_load(configfile)
        return super().__new__(
            cls,
            pipeline=ServerConfig(**config_data['pipeline']),
            adapter=ServerConfig(**config_data['adapter']),
            dir=Dir(**config_data['dir']),
            db=Databases(**config_data['db']),
            logging=Logging(**config_data['logging'])
        )


def load_config():
    """
    Load the floodtags_sdk.yaml in the CONFIG_DIR if available, otherwise load
    the default config of the package
    """
    global config

    config_dir = os.environ.get('CONFIG_DIR')
    if config_dir is not None:
        config_file_loc = Path(config_dir, 'floodtags_sdk.yaml')
        if not config_file_loc.is_file():
            raise FileNotFoundError(
                'The floodtags_sdk.yaml config file does not exist in the'
                ' $CONFIG_DIR directory')
    else:
        raise FileNotFoundError(
            'Cannot find local config file, '
            'CONFIG_DIR environment variable not defined'
        )

    config = Config(config_file_loc)


# Initialize config
load_config()
