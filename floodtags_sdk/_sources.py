# -*- coding: utf-8 -*-
"""
FloodTags filtersources data functions
"""
import json
from pathlib import Path
import logging
import copy
from urllib.parse import quote_plus

from pymongo import MongoClient

from floodtags_sdk import config

logger = logging.getLogger(__name__)


class OfflineSources:
    """
    Offline sources data (used for local tests)
    """

    def __init__(self):
        sources_path = Path(
            Path(__file__).absolute().parent,
            'test_sources.json'
        )
        with open(sources_path, 'r', encoding='utf8') as sourcesfile:
            self._data = json.load(sourcesfile)

        self._cache = {}

    def get(self, filtersource_id, from_cache=True):
        """
        Get the configuration data (dict) for a specific filtersource id

        Arguments:
            filtersource_id --- str: The id of the filtersource

            from_cache=True --- bool: Whether to retrieve the filtersource data
            from the memory cache

        Returns:
            dict --- The configuration data for the filter-source

        Please not that for offline sources, filtersource data is already
        cached when the script is started. Only difference is that the
        set_local attributes are not provided, when from_cache is False. For
        production, please refrain from using from_cache, since then each time
        this function is run, the database is queried.
        """
        if from_cache:
            if filtersource_id in self._cache:
                return self._cache[filtersource_id]

        sdata = [s for s in self._data if s['id'] == filtersource_id]
        if sdata == []:
            raise ValueError(
                'Given filtersource id not in local filtersource config'
            )
        else:
            cached = copy.deepcopy(sdata[0])
            # Store in cache for easy lookup, and to store set_local
            # atttributes
            self._cache[cached['id']] = cached
            return cached

    def get_list(self, has_keys=None):
        """
        Returns the full list of sources

        Arguments:
            has_keys=None --- list: A list of the keys a source should have, in
            order to be returned
        """
        if has_keys is None:
            has_keys = []  # never use mutable objects as default params
        # Return cached variants when available
        sl = []
        for s in self._data:
            for k in has_keys:
                if k not in s:
                    break
            else:
                s_id = s['id']
                if s_id in self._cache:
                    sl.append(self._cache[s_id])
                else:
                    sl.append(s)  # TODO: for Mongo, cache this

        return sl

    def set_local(self, filtersource_id, key, value):
        """
        Add an additional key/value pair to the filtersource data that's
        cached in memory. Usefull e.g. for storing re-usable compiled regular
        expressions and the likes.

        Arguments:
            filtersource_id --- str: The filtersource to store the data for

            key --- str: The key to store data under (Can also be existing)

            value --- any: The data stored for the key (any python type)
        """
        if filtersource_id not in self._cache:
            self.get(filtersource_id, from_cache=False)  # caches the data

        self._cache[filtersource_id][key] = value


class Sources:
    """
    Online filtersources data, from MongoDB
    """

    def __init__(self):
        mongo_url = 'mongodb://{uname}:{passwd}@{url}'.format(
            uname=quote_plus(config.db.settings.username),
            passwd=quote_plus(config.db.settings.password),
            url=config.db.settings.url
        )
        self._client = MongoClient(mongo_url)

        # For now, it just gets all sources at start
        self._db = self._client['floodtags-server-settings']['filtersources']
        self._cache = {}
        for s in self._db.find(None, {'_id': False}):
            self._cache[s['id']] = s

    def get(self, filtersource_id, from_cache=True):
        """
        Get the configuration data (dict) for a specific filtersource id

        Arguments:
            filtersource_id --- str: The id of the filtersource

            from_cache=True --- bool: Whether to retrieve the filtersource data
            from the memory cache

        Returns:
            dict --- The configuration data for the filter-source

        Filtersource data is already cached when the script is started. Only
        difference is that the set_local attributes are not provided, when
        from_cache is False. For production, please refrain from using
        from_cache, since then each time this function is run, the database is
        queried.
        """
        if from_cache:
            if filtersource_id in self._cache:
                return self._cache[filtersource_id]

        s = self._db.find_one({'id': filtersource_id}, {'_id': False})
        if s is None:
            raise ValueError(
                'Given filtersource id not in local filtersource config'
            )
        else:
            self._cache[s['id']] = s
            return s

    def get_list(self, has_keys=None):
        """
        Returns the full list of sources (from cache)

        Arguments:
            has_keys=None --- list: A list of the keys a source should have, in
            order to be returned
        """
        if has_keys is None:
            has_keys = []  # never use mutable objects as default params
        # Return cached variants when available
        sl = []
        for sid, sdata in self._cache.items():
            for k in has_keys:
                if k not in sdata:
                    break
            else:
                sl.append(sdata)

        return sl

    def set_local(self, filtersource_id, key, value):
        """
        Add an additional key/value pair to the filtersource data that's
        cached in memory. Usefull e.g. for storing re-usable compiled regular
        expressions and the likes.

        Arguments:
            filtersource_id --- str: The filtersource to store the data for

            key --- str: The key to store data under (Can also be existing)

            value --- any: The data stored for the key (any python type)
        """
        if filtersource_id not in self._cache:
            self.get(filtersource_id, from_cache=False)  # caches the data

        self._cache[filtersource_id][key] = value


# If a database is configured, use the online version, otherwise use local data
if config.db.settings.url is not None:
    sources = Sources()
else:
    logger.warning('Using local test_sources.json, because the sources '
                   'database is not configured')
    sources = OfflineSources()
