# -*- coding: utf-8 -*-
"""
Contains all tag objects, for each type of source
"""
from collections import abc
import datetime
from pathlib import Path
import copy
import logging
import json
import re

import fastjsonschema

PREFIX_MAPPING = {  # Source-type prefixes for Ids
    'twitter': 't-',
    'whatsapp': 'w-',
    'forum': 'f-',
    'news': 'news-',
    'telegram': 'tg-',
    'bot': 'bot-'
}

DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'  # ISO8601 UTC date format

JSON_SCHEMA_LOCATIONS = {  # Relative to the schemas directory
    "tag": {
        '1.0': 'tag_v1.0.json',
        '1.1': 'tag_v1.1.json',
    },
    "adapter": {
        '0.1': 'adapter_v0.1.json',
        '0.2': 'adapter_v0.2.json'
    },
    "news-event-information-extraction": {
        "0.1": 'extensions/news-event-information-extraction-v0.1.json'
    },
    "flood-image-analysis": {
        "0.1": 'extensions/flood-image-analysis-v0.1.json'
    }
}

# Add the 'latest' ones to the config
for name, version_dict in JSON_SCHEMA_LOCATIONS.items():
    for v, loc in version_dict.items():
        pass
    # Take the latest
    version_dict['latest'] = loc

# Get list of timezones, used for input verification
cur_dir = Path(__file__).absolute().parent
IANA_TIMEZONES = set()
with open(Path(cur_dir, 'iana_timezones.txt'), 'r', encoding='utf8') as\
        tzfile:
    for line in tzfile:
        IANA_TIMEZONES.add(line.strip())


def load_json(file_loc):
    with open(file_loc, 'r', encoding='utf8') as jsonfile:
        return json.load(jsonfile)


schema_dir = Path(cur_dir, 'schemas')

logger = logging.getLogger(__name__)

# Compile regex to convert source names to id
non_letter_chars = re.compile('[^a-zA-Z]*')


def get_version(version_str):
    """
    Converts a string version to tuple, for comparison
    """
    return tuple([int(i) for i in version_str.split('.')])


class _Validator:
    """
    Validator instance, that caches previously compiled validation functions,
    and uses them. Use the .validate() function
    """

    def __init__(self):
        self._validators = {}  # Stores precompiled validators

    def _build_uid(self, type, version, extension=None, extension_version=None):
        """Build a Unique identifier for the validator"""
        if extension is None:
            return '{}_{}'.format(type, version)
        else:
            return '{}_{}_{}_{}'.format(type, version, extension,
                                        extension_version)

    def _load_schema(self, type, version):
        schema_loc = Path(schema_dir, JSON_SCHEMA_LOCATIONS[type][version])
        return load_json(schema_loc)

    def _extend_schema(self, schema, type, version, extension,
                       extension_version):
        """Extends the base schema, with the extension data"""
        if type == 'adapter':
            extension_data = self._load_schema(extension, extension_version)
            defs = extension_data['definitions']
            tag_operations = defs.get('tagOperations')
            evprop_operations = defs.get('eventPropertyOperations')
            if version == '0.1':
                schema_anyof = schema['properties']['payload']['items']['anyOf']
            else:
                schema_anyof = schema['properties']['payload']['anyOf']

            if tag_operations is not None:
                schema_anyof[1]['properties']['operations'] = tag_operations
            else:
                del schema_anyof[1]

            if evprop_operations is not None:
                schema_anyof[0]['properties']['propertyOperations'] =\
                    evprop_operations
            else:
                del schema_anyof[0]

            if len(schema_anyof) == 0:
                raise TypeError('No eventProperty or tag operations defined in'
                                ' extension schema')
            elif len(schema_anyof) == 1:
                # Remove from anyof, since this will give better debug messages
                if version == '0.1':
                    schema['properties']['payload']['items'] = schema_anyof[0]
                else:
                    schema['properties']['payload'] = schema_anyof[0]

    def _compile_validator(self, type, version, extension, extension_version):
        """Compile the required validator, and store it"""
        uid = self._build_uid(type, version, extension, extension_version)
        base_schema = self._load_schema(type, version)
        if extension is not None:
            # Extend the schema before compiling
            self._extend_schema(base_schema, type, version, extension,
                                extension_version)

        self._validators[uid] = fastjsonschema.compile(base_schema)

    def validate(self, data, type, version, extension=None,
                 extension_version=None):
        """
        Validate the data using a json-schema

        Arguments
            data --- any: The data to validate agains the json-schema

            type --- str: Type of data to validate ('tag' or 'adapter')

            version --- str: The version of the format to use (e.g. 'v1.1')

            extension=None --- str: In case an extension format should be used
            (for type=adapter only)

            extension_version --- str: The version of the extension
        """
        uid = self._build_uid(type, version, extension, extension_version)
        try:
            validate = self._validators[uid]
        except KeyError:
            self._compile_validator(type, version, extension,
                                    extension_version)
            validate = self._validators[uid]

        validate(data)


validator = _Validator()


class Location(dict):
    """
    Base location class (A GeoJSON feature, https://geojson.org/)

    Arguments:
        geometry_type --- str: A GeoJSON feature type

        coordinates --- list[float/list]: The coordinates, the format
        depends on the geometry_type

        loc_type --- str: The type of location ('user_picked' or 'GPS')

        id_ --- str: Unique identifier for the location

        name --- str: Location name

        other_properties --- dict: Other information that should be under
        the 'properties' of the GeoJSON ('type', 'name' and 'id' are reserved,
        using these wil raise an error)
    """

    def __init__(self, geometry_type=None, coordinates=None, loc_type=None,
                 id_=None, name=None, other_properties=None):
        if geometry_type is not None:
            # Add geometry and type=feature
            self.update({
                'type': 'Feature',
                'geometry': {
                    'type': geometry_type,
                    'coordinates': coordinates
                }
            })
            self._data['geometry']['type'] = geometry_type

        if loc_type is not None:
            self.setdefault('properties', {})['type'] = loc_type

        if name is not None:
            self.setdefault('properties', {})['name'] = name

        if id_ is not None:
            self.setdefault('properties', {})['id'] = id_

        # Update properties with other_properties
        if other_properties is not None:
            self.setdefault('properties', {}).update(other_properties)


class TagLocation(Location):
    """
    A location, as used in a Tag

    For use, see Location object. geometry_type, coordinates and loc_type are
    required arguments
    """

    def __init__(self, geometry_type, coordinates, loc_type, **kwargs):
        # If there is no id, generate a geohash one
        id_ = kwargs.pop('id_', None)
        if id_ is None:
            id_ = self._create_id(kwargs['coordinates'])
        super().__init__(geometry_type=geometry_type, coordinates=coordinates,
                         loc_type=loc_type, id_=id_, **kwargs)

    def _create_id(self, coordinates):
        """
        Generates a location id, based on its geohash, or geohash of the first
        point (e.g. for Polygons)
        """
        lat, lon = self._get_coordinates(coordinates)

        id_ = 'geohash-' + self._generate_geohash(lat, lon)

        return id_

    def _get_coordinates(self, list_):
        """
        Get the first point from any type of geometry's coordinates
        """
        if isinstance(list_[0], (int, float)):
            lat, lon = list_[1], list_[0]
        else:
            lat, lon = self._get_coordinates(list_[0])

        return lat, lon

    def _generate_geohash(self, lat, lon, length=9):
        """
        Calculate the geohash of the input latitude/longitude
        https://en.wikipedia.org/wiki/Geohash#Algorithm_and_example
        """
        b32 = '0123456789bcdefghjkmnpqrstuvwxyz'
        lon_window = [-180, 180]
        lat_window = [-90, 90]

        iterations = 5 * length
        geohash = ''

        bits = ''
        for i in range(iterations):
            if i % 2 == 0:
                lon_mid = sum(lon_window) / 2
                if lon < lon_mid:
                    bits += '0'
                    lon_window[1] = lon_mid
                else:
                    bits += '1'
                    lon_window[0] = lon_mid
            else:
                lat_mid = sum(lat_window) / 2
                if lat < lat_mid:
                    bits += '0'
                    lat_window[1] = lat_mid
                else:
                    bits += '1'
                    lat_window[0] = lat_mid

            if (i + 1) % 5 == 0:
                ind_ = int(bits, base=2)
                letter = b32[ind_]
                geohash += letter
                bits = ''

        return geohash


class AuthorLocation(Location):
    """
    The location of an author in a Tag.

    For a description of arguments please see the Location object. Either
    geometry_type & coordinates or name is required.
    """

    def __init__(self, *args, **kwargs):
        # There should be either a geometry, or a name
        if kwargs.get('geometry_type') is None:
            if kwargs.get('name') is None:
                raise TypeError('AuthorLocation: Either a name or a '
                                'geometry should be defined')

        super().__init__(*args, **kwargs)


class Tag(abc.MutableMapping):
    """
    A Tag in the FloodTags system
    """
    _available_versions = set(JSON_SCHEMA_LOCATIONS['tag'].keys())

    def __init__(self, as_version):
        self._data = {
            'filterSources': [],
            'source': {},
            'locations': [],
            'media': {
                'photos': [],
                'videos': [],
                'documents': [],
                'audio': [],
                'urls': []
            }
        }
        self._validate_version(as_version)
        self.version = as_version

    def _validate_version(self, version):
        if version not in self._available_versions:
            raise ValueError('The given version is not supported, following '
                             'versions supported: {}'.format(
                                self._available_versions
                                ))

    @staticmethod
    def _get_input_version(dict_):
        version = (1, 1)

        # Changes 1.0 to 1.1: Added Annotated URLs in media (dicts vs str)
        # New author.name and author.language keys
        # author.id starting with http is now allowed
        for k, v in dict_.get('media', {}).items():
            if len(v) > 0:
                if isinstance(v[0], str):
                    version = (1, 0)

        return version

    @staticmethod
    def _upgrade_version(dict_, from_, to):
        """
        Upgrades the version of the dict_ to a new version
        """
        if from_ <= (1, 0) and to >= (1, 1):
            # Upgrades from 1.0 to 1.1: Added Annotated URLS in media (dicts vs str)
            media = dict_.get('media')
            if media is not None:
                for k, v in media.items():
                    updated_v = [{'url': item} for item in v]
                    media[k] = updated_v

    @staticmethod
    def _downgrade_version(dict_, from_, to):
        """
        Downgrade the version of the dict_ to an old version
        """
        if from_ >= (1, 1) and to <= (1, 0):
            # Revert annotatedURLs (convert the 'url' key to string)
            # Remove author id's starting with http, since it's not supported
            # Remove author.name and author.language
            media = dict_.get('media')
            if media is not None:
                for k, v in media.items():
                    updated_v = [item['url'] for item in v]
                    media[k] = updated_v

            author_data = dict_.get('author')
            if author_data is not None:
                author_data.pop('name', None)
                author_data.pop('language', None)
                author_id = author_data.get('id', '')
                if author_id.startswith('http'):
                    del dict_['author']['id']
                    del dict_['source']['userId']
                if author_data == {}:
                    del dict_['author']

    @classmethod
    def from_dict(cls, dict_, as_version):
        """
        Create a Tag object from a dictionary (dict_) (e.g. parsed from JSON)
        Specify as_version to ensure the interface remains the same with
        updates of this package
        """
        dict_ = copy.deepcopy(dict_)  # Don't edit the input
        instance = cls(as_version=as_version)

        # Determine version of input:
        input_version = instance._get_input_version(dict_)

        as_version = get_version(as_version)
        if as_version < input_version:
            instance._downgrade_version(dict_, input_version, as_version)
        elif as_version > input_version:
            instance._upgrade_version(dict_, input_version, as_version)

        # Validate as new version
        instance._data = dict_
        instance.validate()
        return instance

    def validate(self):
        validator.validate(self._data, 'tag', self.version)

    def to_dict(self, as_version=None):
        self.validate()
        data = copy.deepcopy(self._data)
        if as_version is not None:
            self._validate_version(as_version)
            current_version = get_version(self.version)
            output_version = get_version(as_version)
            if current_version > output_version:
                self._downgrade_version(data, current_version, output_version)
            elif current_version < output_version:
                self._upgrade_version(data, current_version, output_version)

        return copy.deepcopy(self._data)

    def as_version(self, version):
        """
        Returns a copy of this tag, in the specified version
        """
        instance = self.__class__(as_version=version)

        # Get version of input:
        input_version = get_version(self.version)

        data = copy.deepcopy(self._data)

        as_version = get_version(version)
        if as_version < input_version:
            instance._downgrade_version(data, input_version, version)
        elif as_version > input_version:
            instance._upgrade_version(data, input_version, version)

        # Validate as new version
        instance._data = data
        instance.validate()
        return instance

    @property
    def date(self):
        """
        datetime.datetime: Convenience property for reading and setting date
        """
        date_str = self._data.get('date')
        return datetime.datetime.strptime(date_str, DATE_FORMAT).replace(
            tzinfo=datetime.timezone.utc
        )

    @date.setter
    def date(self, value):
        if value.tzinfo is not None:
            # First convert to UTC
            date_str = value.astimezone(datetime.timezone.utc).strftime(
                DATE_FORMAT
            )
        else:
            date_str = value.strftime(DATE_FORMAT)
        self._data['date'] = date_str

    def _set_id_prefix(self):
        """Set the id prefix internally"""
        try:
            id_prefix = PREFIX_MAPPING[self._data['source']['type']]
        except KeyError:
            raise ValueError('Before specifying id, first the source.type and'
                             ' when applicable source.name should be set')

        source_name = self._data['source'].get('name')
        if source_name is not None:
            id_prefix += non_letter_chars.sub('', source_name) + '-'

        self._id_prefix = id_prefix

    def set_source_id(self, value):
        """
        Convenience function for setting the source.id, since this has to align
        with id
        """
        if not hasattr(self, '_id_prefix'):
            self._set_id_prefix()

        self._data['id'] = self._id_prefix + value
        self._data['source']['id'] = value

    def set_user_id(self, value):
        author_data = self._data.setdefault('author', {})
        if value.startswith('phone-') or value.startswith('http'):
            # Since it will always be a UID
            author_data['id'] = value
        else:
            if not hasattr(self, '_id_prefix'):
                self._set_id_prefix()
            author_data['id'] = self._id_prefix + value
        self._data.setdefault('source', {})['userId'] = value

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __delitem__(self, key):
        del self._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __repr__(self):
        return repr(self._data)
