# -*- coding: utf-8 -*-
"""
Contains the functions related to sending data to the pipeline
"""
from pathlib import Path
import logging
import json

import requests

from floodtags_sdk import config
from floodtags_sdk.formats import Tag
from floodtags_sdk._aux import save_json

logger = logging.getLogger(__name__)

url = 'http://{ip}:{port}/'.format(ip=config.pipeline.ip,
                                   port=config.pipeline.port)


def send(tags, source_id):
    """
    Send a list of tags to the pipeline

    Arguments:
        tags --- list[tags.IngestionTag]: A list of tags, that are instances of
        floodtags_ingestion.tags.IngestionTag

        source_id --- str: The unique identifier of the algorithm sending
        the tags
    """
    # First verify if the data format is correct
    if not isinstance(tags, list):
        raise TypeError('tags should be a list')
    if not isinstance(tags[0], Tag):
        # Serialize all to tags, to make sure it meets the schema requirements
        send_tags = [Tag.from_dict(t, '1.1') for t in tags]
    else:
        send_tags = tags

    try:
        # Now create the json payload and send the data to the pipeline
        payload = {
            'sourceId': source_id,
            'payload': [t.to_dict() for t in send_tags]
        }
        response = requests.post(url,
                                 data=json.dumps(payload),
                                 timeout=config.pipeline.timeout)
        response.raise_for_status()
    except requests.exceptions.RequestException:
        save_path = Path(config.dir.unsent, 'pipeline_unsent.jl')
        logger.exception(
            'Unable to send data to the pipeline, storing it in {}.'
            ' See details below'.format(
                save_path.as_posix()
            )
        )
        save_json(payload, save_path)
