import json


def save_json(data, out_fileloc):
    """
    Store tags locally (if they cannot be send to pipeline)

    Arguments:
        data --- dict: The data that should have been sent to the pipeline
    """
    # Append the payload that cannot be send
    with open(out_fileloc, 'a', encoding='utf8') as jsonlines_file:
        jsonlines_file.write(json.dumps(data, ensure_ascii=False) + '\n')
